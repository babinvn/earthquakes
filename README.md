# Earthquakes

This is a simple demo using Yandex Heatmap, fastify and IRIS seismic data.

![screenshot](screenshot.png)


To start server:

```
npm i
npm start
```

To update IRIS data cache:

```
curl -g 'http://127.0.0.1:3000/api/earthquakes?bounds=[[-90,-180],[90,180]]&limit=30000' > src/ui/demo.json
```
To run Yandex maps you would need to obtain a Yandex API key here https://developer.tech.yandex.ru/services/. You then put it into index.html code at `<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=<your yandex API key here>"></script>`. Restart the server and navigate to `http://127.0.0.1:3000` in your browser

I found it impractical to update data on the fly using YMaps `boundschange` event since any single call to IRIS backend takes 20s to minutes to complete.

## IRIS

For more details about IRIS datasources please check the following links:

- resource list: https://ds.iris.edu/ds/nodes/dmc/tools/#data_types=timeseries
- station lookup: http://ds.iris.edu/ds/nodes/dmc/tools/station_quicklook/
- API docs: https://service.iris.edu/irisws/timeseries/1/
