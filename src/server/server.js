const { join } = require('path')
const eq = require('./earthquakes');

module.exports = async function (fastify, opts) {
  fastify.register(require('fastify-static'), {
    root: join(__dirname, '..', 'ui')
  })

  // curl -g 'http://127.0.0.1:3000/api/earthquakes?bounds=[[45,130],[60,-178]]'
  fastify.get('/api/earthquakes', async (request, reply) => {
    const bounds = JSON.parse(request.query.bounds);
    const limit = request.query.limit? parseInt(request.query.limit): 10000;
    return eq(bounds, limit);
  })
}
