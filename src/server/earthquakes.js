const https = require('https');
const axios = require('axios');
const parser = require('node-html-parser');

const urlbase = 'https://ds.iris.edu';

async function queryEarthquakes(params) {
  return axios.get(`${urlbase}/ieb/evtable.phtml`, { params });
}
function parseTable(tbody) {
  const data = [];
  tbody.querySelectorAll('TR').forEach((tr) => {
    const rec = {};
    tr.querySelectorAll('TD').forEach((td, index) => {
      switch (index) {
        case 0:
          rec.year = parseInt(td.text.trim(), 10);
          break;
        case 1:
          rec.month = parseInt(td.text.trim(), 10);
          break;
        case 2:
          rec.day = parseInt(td.text.trim(), 10);
          break;
        case 3:
          rec.time = td.text.trim();
          break;
        case 4:
          rec.mag = parseFloat(td.text.trim());
          break;
        case 5:
          rec.lat = parseFloat(td.text.trim());
          break;
        case 6:
          rec.long = parseFloat(td.text.trim());
          break;
        case 7:
          rec.depth = parseFloat(td.text.trim());
          break;
        case 9:
          rec.irisid = td.text.trim();
          break;
        case 10:
          rec.timestamp = parseInt(td.text.trim(), 10);
          break;
        default:
      }
    });
    data.push(rec);
  });
  return data;
}
function parseData(html) {
  const root = parser.parse(html);
  let data;
  root.querySelectorAll('#theBody').forEach((tbody) => {
    data = parseTable(tbody);
  });
  return data;
}
async function getData(bounds, limit) {
  const params = {
    caller: 'IEB',
    st: '1970-01-01',
    et: '2025-01-01',
    orderby: 'time-desc',
    src: 'usgs',
    limit: limit,
    maxlat: bounds[1][0],
    minlat: bounds[0][0],
    maxlon: bounds[1][1],
    minlon: bounds[0][1],
    zm: 5,
    mt: 'ter',
    title: '',
    stitle: '',
  };
  console.log(params);
  const response = await queryEarthquakes(params);
  return parseData(response.data);
}

module.exports = getData;
