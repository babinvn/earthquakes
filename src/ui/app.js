function getHeatmap(eq, ymaps){
  const data = [];
  eq.filter(rec => rec.mag > 3).forEach((rec) => data.push([rec.lat, rec.long]));

  return new ymaps.Heatmap(data, {
    radius: 10,
    dissipating: false,
    opacity: 0.8,
    intensityOfMidpoint: 0.2,
    gradient: {
      0.1: 'rgba(128, 255, 0, 0.7)',
      0.2: 'rgba(255, 255, 0, 0.8)',
      0.7: 'rgba(234, 72, 58, 0.9)',
      1.0: 'rgba(162, 36, 25, 1)',
    },
  });
}

async function init(){
  const { ymaps } = window;
  await ymaps.ready(['Heatmap']);
  const $demo = $('#demo');
  $demo.empty();
  const map = new ymaps.Map(document.getElementById('demo'), {
    center: [52.5, 157.5],
    zoom: 5,
    controls: [],
  });

  const eq = await $.ajax({ url: '/demo.json' });
  console.log(eq.length);
  let heatmap = getHeatmap(eq, ymaps);
  heatmap.setMap(map);

/*
  map.events.add('boundschange', async (e) => {
    const eq = await $.ajax({ url: '/api/earthquakes', data: { bounds: JSON.stringify(e.originalEvent.newBounds) } });
    heatmap.destroy();
    heatmap = getHeatmap(eq, ymaps);
    heatmap.setMap(map);
  });
*/
}

document.addEventListener("DOMContentLoaded", () => {
  init();
});
